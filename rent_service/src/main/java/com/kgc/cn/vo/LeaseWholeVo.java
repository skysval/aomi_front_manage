package com.kgc.cn.vo;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;
import java.util.List;

@ApiModel("房屋整租")
public class LeaseWholeVo {

    @ApiModelProperty("用户id")
    private String uid;

    @ApiModelProperty("小区所在区域")
    private String area;

    @ApiModelProperty("小区名")
    private String community;

    @ApiModelProperty("建筑面积")
    private Double acreage;

    @ApiModelProperty("厅室")
    private String housetype;

    @ApiModelProperty("用户朝向 东：0 南：1 西：2 北：3")
    private Integer direction;

    @ApiModelProperty("车位 有：0 没有：1")
    private Integer carport;

    @ApiModelProperty("电梯 有：0 没有：1")
    private Integer elevator;

    @ApiModelProperty("月租金")
    private Integer money;

    @ApiModelProperty("是否支持短租 （默认）不支持：0 一个月：1 两个月：2 三个月：3 四个月：4 五个月：5 六个月：6")
    private Integer shortrent;

    @ApiModelProperty("装修情况 无装修：0 普通装修：1 精装修：2 豪华装修：3")
    private Integer fitment;

    @ApiModelProperty("用户id")
    private Integer roomdirection;

    @ApiModelProperty("出租要求")
    private List<Integer> demandList;

    @ApiModelProperty("房屋特点")
    private List<Integer> characteristicList;

    @ApiModelProperty("房屋配置")
    private List<Integer> configurationList;

    @ApiModelProperty("房屋描述")
    private String describe;

    @ApiModelProperty("联系人姓名")
    private String linkman;

    @ApiModelProperty("身份 个人房东：0 经纪人：1")
    private Integer status;

    @ApiModelProperty("联系人手机号")
    private String phone;

    @ApiModelProperty("看房时间")
    private Date watchtime;

    @ApiModelProperty("宜住人数")
    private Integer count;

    @ApiModelProperty("起租时间")
    private Date startrent;

    @ApiModelProperty("小区地址")
    private String address;

    @ApiModelProperty("小区楼层")
    private Integer floor;


    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCommunity() {
        return community;
    }

    public void setCommunity(String community) {
        this.community = community;
    }

    public Double getAcreage() {
        return acreage;
    }

    public void setAcreage(Double acreage) {
        this.acreage = acreage;
    }

    public String getHousetype() {
        return housetype;
    }

    public void setHousetype(String housetype) {
        this.housetype = housetype;
    }

    public Integer getDirection() {
        return direction;
    }

    public void setDirection(Integer direction) {
        this.direction = direction;
    }

    public Integer getCarport() {
        return carport;
    }

    public void setCarport(Integer carport) {
        this.carport = carport;
    }

    public Integer getElevator() {
        return elevator;
    }

    public void setElevator(Integer elevator) {
        this.elevator = elevator;
    }

    public Integer getMoney() {
        return money;
    }

    public void setMoney(Integer money) {
        this.money = money;
    }

    public Integer getShortrent() {
        return shortrent;
    }

    public void setShortrent(Integer shortrent) {
        this.shortrent = shortrent;
    }

    public Integer getFitment() {
        return fitment;
    }

    public void setFitment(Integer fitment) {
        this.fitment = fitment;
    }

    public Integer getRoomdirection() {
        return roomdirection;
    }

    public void setRoomdirection(Integer roomdirection) {
        this.roomdirection = roomdirection;
    }

    public List<Integer> getDemandList() {
        return demandList;
    }

    public void setDemandList(List<Integer> demandList) {
        this.demandList = demandList;
    }

    public List<Integer> getCharacteristicList() {
        return characteristicList;
    }

    public void setCharacteristicList(List<Integer> characteristicList) {
        this.characteristicList = characteristicList;
    }

    public List<Integer> getConfigurationList() {
        return configurationList;
    }

    public void setConfigurationList(List<Integer> configurationList) {
        this.configurationList = configurationList;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getFloor() {
        return floor;
    }

    public void setFloor(Integer floor) {
        this.floor = floor;
    }

    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }

    public String getLinkman() {
        return linkman;
    }

    public void setLinkman(String linkman) {
        this.linkman = linkman;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getWatchtime() {
        return watchtime;
    }

    public void setWatchtime(Date watchtime) {
        this.watchtime = watchtime;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Date getStartrent() {
        return startrent;
    }

    public void setStartrent(Date startrent) {
        this.startrent = startrent;
    }
}
