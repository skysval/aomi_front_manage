package com.kgc.cn.aop;

import org.aspectj.lang.annotation.Pointcut;

public class DSPointcut {

    @Pointcut("execution(public * com.kgc.cn.mapper.*.*(..))")
    public void selectorPointcut(){

    }
}
