package com.kgc.cn.mapper;

import com.kgc.cn.model.Characteristic;
import com.kgc.cn.model.CharacteristicExample;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface CharacteristicMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table characteristic
     *
     * @mbg.generated
     */
    long countByExample(CharacteristicExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table characteristic
     *
     * @mbg.generated
     */
    int deleteByExample(CharacteristicExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table characteristic
     *
     * @mbg.generated
     */
    int insert(Characteristic record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table characteristic
     *
     * @mbg.generated
     */
    int insertSelective(Characteristic record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table characteristic
     *
     * @mbg.generated
     */
    List<Characteristic> selectByExample(CharacteristicExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table characteristic
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") Characteristic record, @Param("example") CharacteristicExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table characteristic
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") Characteristic record, @Param("example") CharacteristicExample example);


    int insertHouseAndCharacteristic(@Param("hid") String hid, @Param("cid") Integer cid);
}