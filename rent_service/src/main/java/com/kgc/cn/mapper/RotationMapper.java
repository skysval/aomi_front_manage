package com.kgc.cn.mapper;

import com.kgc.cn.model.Rotation;
import com.kgc.cn.model.RotationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface RotationMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table rotation
     *
     * @mbg.generated
     */
    long countByExample(RotationExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table rotation
     *
     * @mbg.generated
     */
    int deleteByExample(RotationExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table rotation
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table rotation
     *
     * @mbg.generated
     */
    int insert(Rotation record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table rotation
     *
     * @mbg.generated
     */
    int insertSelective(Rotation record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table rotation
     *
     * @mbg.generated
     */
    List<Rotation> selectByExample(RotationExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table rotation
     *
     * @mbg.generated
     */
    Rotation selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table rotation
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") Rotation record, @Param("example") RotationExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table rotation
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") Rotation record, @Param("example") RotationExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table rotation
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(Rotation record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table rotation
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(Rotation record);
}