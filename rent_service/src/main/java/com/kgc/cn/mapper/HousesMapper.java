package com.kgc.cn.mapper;

import com.kgc.cn.model.Houses;
import com.kgc.cn.model.HousesExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface HousesMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table houses
     *
     * @mbg.generated
     */
    long countByExample(HousesExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table houses
     *
     * @mbg.generated
     */
    int deleteByExample(HousesExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table houses
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(String id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table houses
     *
     * @mbg.generated
     */
    int insert(Houses record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table houses
     *
     * @mbg.generated
     */
    int insertSelective(Houses record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table houses
     *
     * @mbg.generated
     */
    List<Houses> selectByExample(HousesExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table houses
     *
     * @mbg.generated
     */
    Houses selectByPrimaryKey(String id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table houses
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") Houses record, @Param("example") HousesExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table houses
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") Houses record, @Param("example") HousesExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table houses
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(Houses record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table houses
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(Houses record);

    int insertCharacteristic(@Param("cid") int cid, @Param("hid") String hid);

    int insertConfiguration(@Param("cfid") int cfid,@Param("hid") String hid);

    int insertDemand(@Param("did")int did,@Param("hid")String hid);
}