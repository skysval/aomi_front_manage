package com.kgc.cn.enums;

/**
 * @Date 2019/12/2 8:50
 * @Creat by admin
 */
public enum  SuccessEnum {
    ADD_SUCCESS("添加成功");


    private String success;

    SuccessEnum(String success){
     this.success = success;
    }


    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }
}
