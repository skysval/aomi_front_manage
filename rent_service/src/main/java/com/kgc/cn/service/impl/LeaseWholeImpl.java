package com.kgc.cn.service.impl;

import com.google.common.collect.Maps;
import com.kgc.cn.enums.Enums;
import com.kgc.cn.mapper.*;
import com.kgc.cn.model.*;
import com.kgc.cn.service.LeaseWholeServcie;
import com.kgc.cn.utils.id.QueryHouseId;
import com.kgc.cn.vo.LeaseDetailsVo;
import com.kgc.cn.vo.LeaseWholeVo;
import org.apache.commons.lang.ObjectUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class LeaseWholeImpl implements LeaseWholeServcie {

    @Autowired
    private DemandMapper demandMapper;
    @Autowired
    private ConfigurationMapper configurationMapper;
    @Autowired
    private CharacteristicMapper characteristicMapper;
    @Autowired
    private PictureMapper pictureMapper;
    @Autowired
    private HousesMapper housesMapper;



    @Override
    @Transactional
    public int addLeaseWhole(LeaseWholeVo leaseWholeVo) {
        Houses houses = new Houses();
        BeanUtils.copyProperties(leaseWholeVo, houses);
        houses.setType(1);
        HousesExample housesExample = new HousesExample();
        housesExample.createCriteria().andAreaEqualTo(houses.getArea()).andCommunityEqualTo(houses.getCommunity());
        if (!CollectionUtils.isEmpty(housesMapper.selectByExample(housesExample))) {
            return Enums.CommonEnum.LEASE_ALEARY_FAIL.getCode();
        }
        String hid = QueryHouseId.queryId();
        houses.setId(hid);
        if (housesMapper.insertSelective(houses) > 0) {
            leaseWholeVo.getCharacteristicList().forEach(characteristics -> {
                characteristicMapper.insertHouseAndCharacteristic(hid, characteristics);
            });
            leaseWholeVo.getConfigurationList().forEach(configurations -> {
                configurationMapper.insertHouseAndConfiguration(hid, configurations);
            });
            leaseWholeVo.getDemandList().forEach(demands -> {
                demandMapper.insertHouseAndDemand(hid, demands);
            });
            return 200;
        }
        return Enums.CommonEnum.LEASE_ADD_FAIL.getCode();

    }

    @Override
    public LeaseDetailsVo showLeaseDetails() {
        CharacteristicExample characteristicExample = new CharacteristicExample();
        Map<String, List<Characteristic>> characteristicMap = Maps.newHashMap();
        characteristicMap.put(Enums.CommonEnum.CHARACTERISTIC, characteristicMapper.selectByExample(characteristicExample));
        ConfigurationExample configurationExample = new ConfigurationExample();
        Map<String, List<Configuration>> configurationMap = Maps.newHashMap();
        configurationMap.put(Enums.CommonEnum.CONFIGURATIONN, configurationMapper.selectByExample(configurationExample));
        DemandExample demandExample = new DemandExample();
        Map<String, List<Demand>> demandMap = Maps.newHashMap();
        demandMap.put(Enums.CommonEnum.DEMAND, demandMapper.selectByExample(demandExample));
        LeaseDetailsVo leaseDetailsVo = new LeaseDetailsVo(demandMap, configurationMap, characteristicMap);
        return leaseDetailsVo;
    }

    @Override
    @Transactional
    public int insertHousePicture(String hid, List<String> pictureUrl) {
       pictureUrl.forEach(pictures ->{
           Picture picture = new Picture(hid,pictures);
           pictureMapper.insert(picture);
       });
       return 200;
    }
}
