package com.kgc.cn.service;

import com.kgc.cn.vo.*;

import java.util.List;

public interface HouseService {
    // 分页查询租房信息
    PageInfo queryHouseWithPage(int pageNo, int pageSize, HouseQueryVo houseQueryVo);

    // 查询轮播图片地址
    List<RotationQueryVo> queryRotationChart();

    // 返回区域字段
    List<String> queryArea();

    // 返回价格范围
    List<MoneyScope> queryMoney();

    // 返回户型字段
    Apartment queryApartment();

    // 返回筛选字段
    Screen queryScreen();
}
