package com.kgc.cn.service;


import com.kgc.cn.vo.LeaseDetailsVo;
import com.kgc.cn.vo.LeaseWholeVo;

import java.util.List;
import java.util.Map;

public interface LeaseWholeServcie {

    /**
     * 发布房屋信息
     *
     * @param leaseWholeVo
     * @return
     */
    int addLeaseWhole(LeaseWholeVo leaseWholeVo);


    /**
     * 展示出租房屋信息要求
     *
     * @return
     */
    LeaseDetailsVo showLeaseDetails();

    /**
     * 上传房屋图片
     * @param hid
     * @param pictureUrl
     * @return
     */
    int insertHousePicture(String hid,List<String> pictureUrl);


}
