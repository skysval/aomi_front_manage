package com.kgc.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "分页model")
public class PageSearchInfo implements Serializable {
    @ApiModelProperty(value = "当前页")
    int pageNo = 1;
    @ApiModelProperty(value = "每页数量")
    int pageSize = 10;
}
