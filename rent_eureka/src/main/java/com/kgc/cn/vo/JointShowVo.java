package com.kgc.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Date 2020/1/9 10:09
 * @Creat by Crane
 */
@ApiModel("多选项")
public class JointShowVo implements Serializable {
    @ApiModelProperty("房屋特色")
    private List<Characteristic> character;

    @ApiModelProperty("房屋配置")
    private List<Configuration> config;

    @ApiModelProperty("出租要求")
    private List<Demand> demand;

    public List<Characteristic> getCharacter() {
        return character;
    }

    public void setCharacter(List<Characteristic> character) {
        this.character = character;
    }

    public List<Configuration> getConfig() {
        return config;
    }

    public void setConfig(List<Configuration> config) {
        this.config = config;
    }

    public List<Demand> getDemand() {
        return demand;
    }

    public void setDemand(List<Demand> demand) {
        this.demand = demand;
    }
}
