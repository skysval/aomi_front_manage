package com.kgc.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "查找租房信息用model")
public class HouseQueryVo implements Serializable {
    @ApiModelProperty(value = "合租类型")
    private Integer type;
    @ApiModelProperty(value = "区域")
    private String area;
    @ApiModelProperty(value = "户型（厅室）")
    private String housetype;
    @ApiModelProperty(value = "租金下限")
    private Integer moneyFrom;
    @ApiModelProperty(value = "租金上限")
    private Integer moneyTo;
    @ApiModelProperty(value = "搜索地区范围/小区名称/地址")
    private String searchmsg;
    @ApiModelProperty(value = "排序方式,0:默认，1：最新发布，2：价格从低到高，3：价格从高到低，4：面积从小到大，5：面积从大到小")
    private Integer sortWayCode = 0;
    @ApiModelProperty(hidden = true)
    private String sortWay;
    @ApiModelProperty(value = "朝向")
    private Integer direction;
    @ApiModelProperty(value = "出租要求")
    private String demand;
    @ApiModelProperty(value = "房源亮点")
    private String characteristic;

    public void setSortWay() {
        switch (sortWayCode) {
            case 1:
                this.sortWay = "ORDER BY uptime DESC ";
                break;
            case 2:
                this.sortWay = "ORDER BY money";
                break;
            case 3:
                this.sortWay = "ORDER BY money DESC";
                break;
            case 4:
                this.sortWay = "ORDER BY acreage";
                break;
            case 5:
                this.sortWay = "ORDER BY acreage DESC";
                break;
            default:
                this.sortWay = null;
                break;
        }
    }

    public String getSortWay() {
        return sortWay;
    }
}
