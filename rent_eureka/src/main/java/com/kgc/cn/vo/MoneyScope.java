package com.kgc.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
@ApiModel(value = "价格范围")
public class MoneyScope implements Serializable {
    @ApiModelProperty(value = "价格范围信息")
    private String msg;
    @ApiModelProperty(value = "价格下限")
    private Integer moneyFrom;
    @ApiModelProperty(value = "价格上限")
    private Integer moneyTo;
}
