package com.kgc.cn.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

@ApiModel(value = "轮播图model")
public class RotationQueryVo implements Serializable {
    @ApiModelProperty(value = "轮播图地址")
    private String uri;
    @ApiModelProperty(value = "轮播图指向地址")
    private String url;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
