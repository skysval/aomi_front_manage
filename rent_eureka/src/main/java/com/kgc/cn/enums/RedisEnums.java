package com.kgc.cn.enums;

import lombok.Getter;

@Getter
public enum RedisEnums {
    STOCK_NAME_SPACE("stock:"),
    WAIT_NAME_SPACE("wait"),
    REDIS_LOCK("lock");

    private String name;


    RedisEnums(String name) {
        this.name = name;
    }
}
