package com.kgc.cn.enums;

import lombok.Getter;

public interface Enums {

    int getCode();

    String getMsg();

    @Getter
    enum CommonEnum implements Enums {
        LOGIN_ERROR(10000, "账号或密码错误，登录失败"),
        UPDATE_ERROR(10009, "删除失败"),
        EID_NOT_EXIST(10000, "工号不存在"),
        DATE_ERROR(100088, "日期格式不正确"),
        DAY_ERROR(1000089, "请假天数必须大于0");

        private int code;
        private String msg;

        public static final String OUT_SUCCESS = "注销成功";
        public static final String DEL_SUCCESS = "删除成功";
        public static final String TOKEN_NAME_SPACE = "token:";
        public static final String STAFF_TOKEN = "staff_token";
        public static final String VACATION_SUCCESS = "请假成功";


        CommonEnum(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }
    }

    @Getter
    enum FileEnum implements Enums {
        FILE_NOT_EXIST(20009, "文件不存在，请重新上传");
        private int code;
        private String msg;

        FileEnum(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }

    }

    @Getter
    enum EmployeeEnum implements Enums {
        EMPLOYEE_IS_FREEZE(30008, "此员工账号已冻结"),
        UPDATE_ERROR(30004, "修改员工信息失败"),
        QUERY_ERROR(30003, "没有查到相关员工信息"),
        DEL_ERROR(30002, "删除员工失败"),
        FREEZE_ERROR(30009, "冻结员工账号失败"),
        EMPLOYEE_NOT_EXIST(30001, "该账号不存在"),
        EMPLOYEE_ADD_FAIL(30010, "员工添加失败"),
        UPDATE_LEVEL_FAIL(30011, "权限不足，无法修改"),
        LOGINOUTTIME_FAIL(30012, "登录失效，请重新登录"),
        RECORD_ERROR(30007, "恢复失败"),
        UPDATEPASSWORD_FAIL(30013, "修改密码失败");
        private int code;
        private String msg;

        public static final String FREEZE_SUCCESS = "冻结员工账号成功";
        public static final String DEL_SUCCESS = "删除员工成功";
        public static final String UPDATE_SUCCESS = "修改员工信息成功";
        public static final String EMPLOYEE_ADD_SUCCESS = "添加员工成功";
        public static final String UPDATEPASSWORD_SUCCESS = "修改密码成功，请重新登录";
        public static final String RECORD_SUCCESS = "恢复成功";


        public static final String EMPLOYEE_ADD_SUCESS = "添加员工成功";

        EmployeeEnum(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }

    }

    @Getter
    enum GoodsEnum implements Enums {
        ADD_DISCOUNT_ERROR(40009, "添加商品折扣失败"),
        ADD_GOODS_FAIL(40010, "添加商品失败"),
        SELECT_FAIL(10085, "查询失败，没有该商品"),
        UPDATE_FAIL(10086, "修改商品失败"),
        DELETE_FAIL(10087, "删除失败"),
        UPDATE_DISCOUNT_FAIL(10088, "修改商品折扣失败"),
        NOTDISCOUNT_FAIL(10089, "该商品没有打折活动");

        private int code;
        private String msg;

        public static final String ADD_DISCOUNT_SUCCESS = "添加商品折扣成功";
        public static final String ADD_GOODS_SUCCESS = "添加商品成功";
        public static final String UPDATE_SUCCESS = "修改商品成功";
        public static final String DELETE_SUCCESS = "删除商品成功";
        public static final String ADD_DISCOUNT_FAIL = "discount exist";
        public static final String GOODSDISCONT_UPDATE_SUCCESS = "修改商品折扣成功";

        GoodsEnum(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }

    }
}
