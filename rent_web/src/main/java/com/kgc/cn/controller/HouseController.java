package com.kgc.cn.controller;

import com.google.common.collect.Lists;
import com.kgc.cn.service.HouseService;
import com.kgc.cn.config.file.FileUpdateProperties;
import com.kgc.cn.utils.ReturnResult;
import com.kgc.cn.utils.ReturnResultUtils;
import com.kgc.cn.vo.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Api(tags = "主页")
@RestController
@RequestMapping(value = "/house")
public class HouseController {

    @Autowired
    private HouseService houseService;

    @Autowired
    private FileUpdateProperties fileUpdateProperties;

    @ApiOperation(value = "租房信息")
    @PostMapping(value = "/main")
    public ReturnResult<PageInfo> mainPage(@Valid PageSearchInfo pageSearchInfo,
                                           @Valid HouseQueryVo houseQueryVo) {
        return ReturnResultUtils.returnSuccess(houseService.mainPage(pageSearchInfo.getPageNo(), pageSearchInfo.getPageSize(), houseQueryVo));
    }

    @ApiOperation(value = "轮播图")
    @PostMapping(value = "/rotation")
    public ReturnResult<List<RotationQueryVo>> rotationChart(){
        return ReturnResultUtils.returnSuccess(houseService.queryRotationChart());
    }

    @ApiOperation(value = "区域")
    @PostMapping(value = "/area")
    public ReturnResult<List<String>> queryArea() {
        return ReturnResultUtils.returnSuccess(houseService.queryArea());
    }


    @ApiOperation(value = "租金")
    @PostMapping(value = "/money")
    public ReturnResult<List<MoneyScope>> queryMoney() {
        return ReturnResultUtils.returnSuccess( houseService.queryMoney());
    }

    @ApiOperation(value = "户型")
    @PostMapping(value = "/apartment")
    public ReturnResult<Apartment> queryApartment() {
        return ReturnResultUtils.returnSuccess(houseService.queryApartment());
    }

    @ApiOperation(value = "筛选")
    @PostMapping(value = "/screen")
    public ReturnResult<Screen> queryScreen() {
        return ReturnResultUtils.returnSuccess(houseService.queryScreen());
    }

}
