package com.kgc.cn.enums;

import lombok.Getter;

public interface Enums {

    int getCode();

    String getMsg();

    @Getter
    enum CommonEnum implements Enums {
        UPDATE_ERROR(10009, "删除失败"),
        HOUSE_FAIL(18888, "该房屋已下架");

        private int code;
        private String msg;

        public static final String DEL_SUCCESS = "删除成功";
        public static final String DELL_SUCCESS = "举报成功";

        CommonEnum(int code, String msg) {
            this.code = code;
            this.msg = msg;
        }
    }
}
