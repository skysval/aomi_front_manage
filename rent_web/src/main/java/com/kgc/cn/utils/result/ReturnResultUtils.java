package com.kgc.cn.utils.result;

import com.kgc.cn.enums.FailEnum;
import com.kgc.cn.enums.SuccessEnum;

/***
 *
 * 统一返回工具类
 */
public class ReturnResultUtils{

    /***
     * 成功 不带数据
     * @return
     */
    public static ReturnResult returnSuccess(SuccessEnum e){
        ReturnResult returnResult=new ReturnResult();
        returnResult.setCode(0);
        returnResult.setMessage(e.getSuccess());
        return returnResult;
    }
    /***
     * 成功 带数据
     * @return
     */
    public static ReturnResult returnSuccess(Object data){
        ReturnResult returnResult=new ReturnResult();
        returnResult.setMessage("success");
        returnResult.setCode(0);
        returnResult.setData(data);
        return returnResult;
    }
    /***
     * 失败
     * 不带数据
     * @return
     */
    public static ReturnResult returnFail(FailEnum code){
        ReturnResult returnResult=new ReturnResult();
        returnResult.setCode(code.getCode());
        returnResult.setMessage(code.getMsg());
        return returnResult;
    }

    /***
     * 失败
     * 带数据
     * @return
     */
    public static ReturnResult returnFail(Object e){
        ReturnResult returnResult=new ReturnResult();
        returnResult.setData(e);
        return returnResult;
    }
}
