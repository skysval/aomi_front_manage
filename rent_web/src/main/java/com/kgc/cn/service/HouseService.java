package com.kgc.cn.service;

import com.kgc.cn.utils.returns.ReturnResult;
import com.kgc.cn.vo.*;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(name = "rent-service", fallback = HouseService.HouseServiceImpl.class)
public interface HouseService {

    @PostMapping(value = "/house/main")
    PageInfo mainPage(@RequestParam(value = "pageNo") int pageNo,
                      @RequestParam(value = "pageSize") int pageSize,
                      @RequestBody HouseQueryVo houseQueryVo);

    @PostMapping(value = "/house/rotation")
    List<RotationQueryVo> queryRotationChart();

    @PostMapping(value = "/house/area")
    public List<String> queryArea();

    @PostMapping(value = "/house/rotation")
    public List<RotationQueryVo> RotationChart();

    @PostMapping(value = "/house/money")
    public List<MoneyScope> queryMoney();

    @PostMapping(value = "/house/apartment")
    public Apartment queryApartment();

    @PostMapping(value = "/house/screen")
    public Screen queryScreen();

    @Component
    class HouseServiceImpl implements HouseService {

        @Override
        public PageInfo mainPage(int pageNo, int pageSize, HouseQueryVo houseQueryVo) {
            return null;
        }

        @Override
        public List<RotationQueryVo> queryRotationChart() {
            return null;
        }

        @Override
        public List<String> queryArea() {
            return null;
        }

        @Override
        public List<RotationQueryVo> RotationChart() {
            return null;
        }

        @Override
        public List<MoneyScope> queryMoney() {
            return null;
        }

        @Override
        public Apartment queryApartment() {
            return null;
        }

        @Override
        public Screen queryScreen() {
            return null;
        }
    }
}
